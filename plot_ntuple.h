#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include "TLorentzVector.h"
#include <stdlib.h>
#include <algorithm>

#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TBranch.h"
#include "TChain.h"
#include "TLeaf.h"
#include "TMath.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include <sstream>
#include <iomanip>

TTree * tree;

vector<TString> v_var;
vector<TString> v_name;
vector<int> v_nbins;
vector<int> v_nmin;
vector<int> v_nmax;
vector<int> v_GeV;
vector<int> v_int_or_float;

Float_t weight;
Int_t m_var_int;
vector<float>* m_var;

void Variables_and_bins(TString Truth);
